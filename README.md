# Code compilation and MPI - Exercises

This repository contains all the exercises of the course "Code compilation and
MPI" given by SCITAS at EPFL. You can find the list, description and slides of
all the courses here: 
[[https://www.epfl.ch/research/facilities/scitas/documentation/training/ |
https://www.epfl.ch/research/facilities/scitas/documentation/training/ ]]


