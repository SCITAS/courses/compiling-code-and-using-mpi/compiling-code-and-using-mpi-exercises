#include <algorithm>
#include <assert.h>
#include <chrono>
#include <iostream>
#include <vector>

void daxpy(double a, const std::vector<double> &x, std::vector<double> &y) {
  assert(x.size() == y.size() && "The vector sizes do not match");

  for (size_t i = 0; i < y.size(); i++) {
    y[i] = a * x[i] + y[i];
  }
}

double max_value(const std::vector<double> &x) {
  assert(not x.empty() && "The vector is empty");

  auto max_it = std::max_element(x.begin(), x.end());

  return *max_it;
}

int main() {
  using chrono = std::chrono::steady_clock;

  const size_t n = 100000000;

  /* Create and init the vectors */
  std::vector<double> x(n, 1.0);
  std::vector<double> y(n, 2.0);

  double a = 2.0;

  const auto start{chrono::now()};

  /* Call daxpy */
  daxpy(a, x, y);

  const auto end{chrono::now()};
  const std::chrono::duration<double, std::milli> elapsed_ms{end - start};
  std::printf("timing for daxpy C++: %f ms\n", elapsed_ms.count());
  std::printf("max value of z: %f\n", max_value(y));

  return 0;
}
