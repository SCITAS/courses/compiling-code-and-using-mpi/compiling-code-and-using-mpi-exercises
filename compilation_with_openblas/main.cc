#include <algorithm>
#include <chrono>
#include <iostream>
#include <vector>

#include <cblas.h>

double max_value(const std::vector<double> &x) {
  auto max_it = std::max_element(x.begin(), x.end());

  return *max_it;
}

int main() {
  using chrono = std::chrono::steady_clock;

  const size_t n = 100000000;

  /* Create and init the vectors */
  std::vector<double> x(n, 1.0);
  std::vector<double> y(n, 2.0);

  double a = 2.0;

  const auto start{chrono::now()};

  /* Call daxpy */
  cblas_daxpy(x.size(), a, x.data(), 1, y.data(), 1);

  const auto end{chrono::now()};
  const std::chrono::duration<double, std::milli> elapsed_ms{end - start};
  std::printf("timing for daxpy C++: %f ms\n", elapsed_ms.count());
  std::printf("max value of z: %f\n", max_value(y));

  return 0;
}
